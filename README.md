# A simple todo app with spring and svelte


[docker image](https://hub.docker.com/repository/docker/erikyy/spring-todo-docker)

run: 

`docker run -p 8080:8080 erikyy/spring-todo-docker`

## Development with docker
### Requirements 
`Docker`

`docker-compose`

### Linux
first cd into frontend folder then run `npm run build && npm run postinstall` to copy build files to static folder in demo-docker app.

Then run these: 

`sh build.sh`

`docker build -t erikyy/spring-todo-docker . `

`docker run -p 8080:8080 erikyy/spring-todo-docker`

## Development Backend
### Requirements 
`OpenJDK-15`


Before running docker build, run build.sh to generate jar file, then run 
### Linux
`sh build.sh`

`java -jar appname.jar` : in build/libs folder

## Development Frontend
### Requirements
`npm/node`

### Linux
`npm install`

`npm run dev`

### Deploy/Copy files
`npm run postinstall`

