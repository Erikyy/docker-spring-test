package ee.erik.demodocker.repository;

import org.springframework.data.repository.CrudRepository;

import ee.erik.demodocker.model.Todo;

public interface TodoRepository extends CrudRepository<Todo, Long> {
    
}
