package ee.erik.demodocker.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import ee.erik.demodocker.model.Todo;
import ee.erik.demodocker.repository.TodoRepository;

@RestController
@RequestMapping("/api/v1")
public class TodoController {
    
    @Autowired
    private TodoRepository repository;

    @GetMapping("/todos")
    public List<Todo> test() {
        List<Todo> todos = new ArrayList<>();
        repository.findAll().forEach(todo -> todos.add(todo));
        return todos;
    }

    @PostMapping("/add")
    public void add(@RequestParam String name) {
        Todo todo = new Todo();
        todo.setName(name);
        repository.save(todo);
    }

    @PostMapping("/delete")
    public void delete(@RequestParam Long id) {
        repository.deleteById(id);
    }
}