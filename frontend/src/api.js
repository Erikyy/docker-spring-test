import axios from "axios";

export default class API {
    constructor () {

    }

    async getTodos() {
        const data = await axios.get('http://localhost:8080/api/v1/todos');
        return data.data;
    }

    async postTodo(name) {
        axios.post('http://localhost:8080/api/v1/add?name=' + name);
        window.location.reload()
    }

    async deleteTodo(id) {
        axios.post('http://localhost:8080/api/v1/delete?id=' + id);
        window.location.reload()
    }
}